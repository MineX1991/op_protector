package de.bungeedev.op.main;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author MineX1991
 */
public class MySQL {
  
    public static String username;
    public static String password;
    public static String database;
    public static String host;
    public static String port;
    public static Connection con;
    
    public MySQL(String user, String pass, String host2, String dB) {}
    
    public static void connect() {
        if (!isConnected()) {
            try {
                con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database + "?user=" + username + "&password=" + password + "&autoReconnect=true");
                Bukkit.getConsoleSender().sendMessage("§7[§3MySQL§7] §3the database seccesfully connected!");
            } catch (SQLException e) {
                Bukkit.getConsoleSender().sendMessage("§7[§3MySQL§7] §4ERROR: §cthe connection has been connection error! Please contact the Plugin Developer -> MineX1991");
            }
        }
    }
    public static void close() {
        if (isConnected()) {
            try {
                con.close();
                Bukkit.getConsoleSender().sendMessage("§7[§3MySQL§7] §ethe connection succesfully disconnected!");
            } catch (SQLException e) {
                Bukkit.getConsoleSender().sendMessage("§7[§3MySQL§7] the connection has been disconnect error! Please contact the Plugin Developer -> MineX1991");
            }
        }
    }
    public static boolean isConnected() {
        return con != null;
    }
    public static void createAllowOPTable() {
        if (isConnected()) {
            try {
                con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS OP (UUID VARCHAR(100), NAME VARCHAR(100), OPPerm int)");
                Bukkit.getConsoleSender().sendMessage("§4MySQL MySQL Table created");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    public static void update(String qry) {
        if (isConnected()) {
            try {
                con.createStatement().executeUpdate(qry);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    public static ResultSet getResult(String qry) {
            ResultSet rs = null;
        try {
            Statement st = con.createStatement();
            rs = st.executeQuery(qry);
        } catch (SQLException ex) {
            connect();
            System.err.println(ex);
        }
        return rs;
    }
    public static File getMySQLFile() {
        return new File("plugins//OP_Protector", "MySQL.yml");
    }
    public static FileConfiguration getMySQLFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getMySQLFile());
    }
    public static void setStandardMySQL() {
        try {
            FileConfiguration cfg = getMySQLFileConfiguration();
            
            cfg.options().copyDefaults(true);
            cfg.addDefault("username", "root");
            cfg.addDefault("password", "password");
            cfg.addDefault("database", "localhost");
            cfg.addDefault("host", "localhost");
            cfg.addDefault("port", "3306");
            cfg.save(getMySQLFile());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public static void readMySQL() {
        FileConfiguration cfg = getMySQLFileConfiguration();
        username = cfg.getString("username");
        password = cfg.getString("password");
        database = cfg.getString("database");
        host = cfg.getString("host");
        port = cfg.getString("port");
    }
    public static ResultSet getResult(String string, Double x, Double y, Double z) {
        return null;
    }
}