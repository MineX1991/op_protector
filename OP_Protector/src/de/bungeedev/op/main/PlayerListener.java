package de.bungeedev.op.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/**
 * @author MineX1991
 */
public class PlayerListener implements Listener {
    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        if (p.isOp()) {
            if (OPManager.getOpPerm(p.getUniqueId().toString(), p.getName()).intValue() == 0) {
                if (!PermissionsEx.getUser(p).inGroup("Owner")) {
                    e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§4ERROR: §cDu bist nicht befugt auf dem Server OP zu besitzen, §3und wurdest deswegen vom Server gekickt!");
                    p.setOp(false);
                } else if (PermissionsEx.getUser(p).inGroup("Owner")) {
                    e.allow();
                }
            } else {
                e.allow();
            }
        }
    }
}