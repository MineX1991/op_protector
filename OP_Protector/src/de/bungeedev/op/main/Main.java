package de.bungeedev.op.main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Main extends JavaPlugin {
  public static Main instance;
  
  public void onEnable() {
    instance = this;
    MySQL.readMySQL();
    MySQL.setStandardMySQL();
    MySQL.connect();
    MySQL.createAllowOPTable();
    for (Player all : Bukkit.getOnlinePlayers()) {
      Main.OpPermAllow(all);
    }
    PluginManager pm = Bukkit.getPluginManager();
    pm.registerEvents(new PlayerListener(), this);
    getCommand("allow").setExecutor(new CMD_AllowOP());
  }
  public static void OpPermAllow(Player p) {
    Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, new Runnable() {
      public void run() {
        if((OPManager.playerExists(p.getUniqueId().toString())) && (OPManager.getOpPerm(p.getUniqueId().toString(), p.getName()).intValue() == 0)
                &&(!PermissionsEx.getUser(p).inGroup("Owner"))) {
            p.setOp(false);
        }
      }
    }, 20L, 20L);
  }
}